#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QMessageBox>
#include <QMenu>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setTrayIconActions();
    showTrayIcon();

    m_provider = new SocketProvider();
    connect(m_provider, SIGNAL(commandReceived(QString)), this, SLOT(parseCommand(QString)));
    connect(m_provider, SIGNAL(messToLog(QString)), this, SLOT(toLog(QString)));
    m_provider->moveToThread(&m_providerThread);
    m_providerThread.start();
    emit m_provider->closed();
}

MainWindow::~MainWindow()
{
    m_provider->listenSocketStop();
    m_providerThread.terminate();
    m_providerThread.wait();
    delete ui;
}

void MainWindow::pressKey(uchar code, int timeDelay)
{
    INPUT ev;
    memset(&ev, 0, sizeof(INPUT));
    ev.type = INPUT_KEYBOARD;
    ev.ki.wVk = code;
    ev.ki.wScan = MapVirtualKey(code, 0);
    SendInput(1, &ev, sizeof(INPUT));
    if(timeDelay)
        Sleep(timeDelay);
    ev.ki.dwFlags = KEYEVENTF_KEYUP;
    SendInput(1, &ev, sizeof(INPUT));
}

void MainWindow::downKey(uchar code)
{
    INPUT ev;
    memset(&ev, 0, sizeof(INPUT));
    ev.type = INPUT_KEYBOARD;
    ev.ki.wVk = code;
    ev.ki.wScan = MapVirtualKey(code, 0);
    SendInput(1, &ev, sizeof(INPUT));
}

void MainWindow::upKey(uchar code)
{
    INPUT ev;
    memset(&ev, 0, sizeof(INPUT));
    ev.type = INPUT_KEYBOARD;
    ev.ki.wVk = code;
    ev.ki.wScan = MapVirtualKey(code, 0);
    ev.ki.dwFlags = KEYEVENTF_KEYUP;
    SendInput(1, &ev, sizeof(INPUT));
}

void MainWindow::parseCommand(QString comm)
{
    qDebug() << "\t" << comm;
    if(comm.section("|", 0, 0) == "m")
    {
        int dx = comm.section("|", 1, 1).toInt();
        int dy = comm.section("|", 2, 2).toInt();
        mouse_event(MOUSEEVENTF_MOVE, dx, dy, 0, 0);
    }
    else if(comm == "mlc")
    {
        mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
        mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
    }
    else if(comm == "mrc")
    {
        mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
        mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
    }
    if(comm.section("|", 0, 0) == "k")
    {
        QString key = comm.section("|", 1, 1);
        if(key == "-")
        {
            pressKey(VK_OEM_MINUS, 0);
        }
        else if(key == "+")
        {
            pressKey(VK_OEM_PLUS, 0);
        }
        else if(key == "[")
        {
            pressKey(VK_OEM_4, 0);
        }
        else if(key == "]")
        {
            pressKey(VK_OEM_6, 0);
        }
        else if(key == ";")
        {
            pressKey(VK_OEM_1, 0);
        }
        else if(key == "'")
        {
            pressKey(VK_OEM_7, 0);
        }
        else if(key == ",")
        {
            pressKey(VK_OEM_COMMA, 0);
        }
        else if(key == ".")
        {
            pressKey(VK_OEM_PERIOD, 0);
        }
        else if(key == "/")
        {
            pressKey(VK_OEM_2, 0);
        }
        else if(key == "<")
        {
            pressKey(VK_LEFT, 0);
        }
        else if(key == ">")
        {
            pressKey(VK_RIGHT, 0);
        }
        else if(key.size() == 1)
        {
            pressKey(*(comm.section("|", 1, 1).toStdString().c_str()), 0);
        }
        else if(key == "Enter")
        {
            pressKey(VK_RETURN, 0);
        }
        else if(key == "Back")
        {
            pressKey(VK_BACK, 0);
        }
        else if(key == "Lang")
        {
            downKey(VK_LMENU);
            downKey(VK_SHIFT);
            upKey(VK_LMENU);
            upKey(VK_SHIFT);
        }
        else if(key == "Win")
        {
            pressKey(VK_LWIN, 0);
        }
        else if(key == "Tab")
        {
            pressKey(VK_TAB, 0);
        }
        else if(key == "Esc")
        {
            pressKey(VK_ESCAPE, 0);
        }
        else if(key == "Caps")
        {
            pressKey(VK_CAPITAL, 0);
        }
        else if(key == "Shift")
        {
            if(comm.section("|", 2, 2) == "d")
                downKey(VK_LSHIFT);
            else
                upKey(VK_LSHIFT);
        }
        else if(key == "Alt")
        {
            if(comm.section("|", 2, 2) == "d")
                downKey(VK_LMENU);
            else
                upKey(VK_LMENU);
        }
        else if(key == "Ctrl")
        {
            if(comm.section("|", 2, 2) == "d")
                downKey(VK_CONTROL);
            else
                upKey(VK_CONTROL);
        }
    }
    else if(comm == "play")
    {
        pressKey(VK_SPACE, 0);
    }
    else if(comm == "sp")
    {
        pressKey(VK_VOLUME_UP, 0);
    }
    else if(comm == "sm")
    {
        pressKey(VK_VOLUME_DOWN, 0);
    }

}

void MainWindow::toLog(QString mess)
{
    ui->teLog->append(mess);
    QTextCursor cur = ui->teLog->textCursor();
    cur.movePosition(QTextCursor::End);
    ui->teLog->setTextCursor(cur);
}

void MainWindow::resetService()
{
    m_provider->closeProvider();
    emit m_provider->closed();
}

void MainWindow::changeEvent(QEvent *event)
{
    QMainWindow::changeEvent(event);
    if (event->type() == QEvent::WindowStateChange)
    {
        if(isMinimized())
        {
            this->hide();
        }
    }
}

void MainWindow::trayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch(reason)
    {
    case QSystemTrayIcon::Trigger:
        trayActionExecute();
        break;
    case QSystemTrayIcon::DoubleClick:
        showNormal();
        break;
    default:
        break;
    }
}

void MainWindow::trayActionExecute()
{
    m_trayIcon->showMessage("Last event message", ui->teLog->document()->toPlainText().section("\n", -1, -1));
}

void MainWindow::setTrayIconActions()
{
    // Setting actions
    m_minAction = new QAction("Minimize", this);
    m_restoreAction = new QAction("Restore", this);
    m_resetAction = new QAction("Reset", this);
    m_quitAction = new QAction("Exit", this);

    // Connecting actions to slots
    connect(m_minAction, SIGNAL(triggered()), this, SLOT(hide()));
    connect(m_restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));
    connect(m_resetAction, SIGNAL(triggered()), this, SLOT(resetService()));
    connect(m_quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));

    // Setting system tray's icon menu
    m_trayMenu = new QMenu(this);
    m_trayMenu->addAction(m_minAction);
    m_trayMenu->addAction(m_restoreAction);
    m_trayMenu->addAction(m_resetAction);
    m_trayMenu->addAction(m_quitAction);
}

void MainWindow::showTrayIcon()
{
    m_trayIcon = new QSystemTrayIcon(this);
    QIcon trayImage(":/pics/mouse.png");
    m_trayIcon->setIcon(trayImage);
    m_trayIcon->setContextMenu(m_trayMenu);
    m_trayIcon->setToolTip("PC Remote");

    connect(m_trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(trayIconActivated(QSystemTrayIcon::ActivationReason)));

    m_trayIcon->show();
}
