#pragma once

#include <QObject>
#include <QTextEdit>

#include <Winsock2.h>
#include <Ws2bth.h>
#include <bthsdpdef.h>
// Link to Bthprops.lib
#include <BluetoothAPIs.h>
#include <initguid.h>

// FE82AE3D-809C-4A85-8A3B-1C91AD2956A9
//DEFINE_GUID(PCControlService_UUID, 0xFE82AE3D, 0x809C, 0x4A85, 0x8A, 0x3D, 0x1C, 0x91, 0xAD, 0x29, 0x56, 0xA9);

class SocketProvider : public QObject
{
    Q_OBJECT
public:
    explicit SocketProvider(QObject *parent = 0);

    ~SocketProvider();

signals:
    void commandReceived(QString comm);
    void messToLog(QString mess);
    void accepted();
    void closed();

public slots:
    void initProvider();
    void closeProvider();
    void listenSocket();
    void listenSocketStop();

private:
    bool m_working;
    SOCKET m_portSocket, m_transfSocket;
    WSAQUERYSET m_service;
};

