#include "socketprovider.h"

#include <QDebug>

SocketProvider::SocketProvider(QObject *parent) :
    QObject(parent),
    m_working(true)
{
    connect(this, SIGNAL(accepted()), this, SLOT(listenSocket()));
    connect(this, SIGNAL(closed()), this, SLOT(initProvider()));
}

SocketProvider::~SocketProvider()
{
    closeProvider();
}

void SocketProvider::initProvider()
{
    m_working = true;

    WORD wVersionRequested = 0x202;
    WSADATA m_data;
    if (0 == WSAStartup(wVersionRequested, &m_data))
    {
        m_portSocket = socket(AF_BTH, SOCK_STREAM, BTHPROTO_RFCOMM);
        const DWORD lastError = ::GetLastError();

        if (m_portSocket == INVALID_SOCKET)
        {
            emit messToLog(tr("Failed to get bluetooth socket! %s").arg(lastError));
            return;
        }
        WSAPROTOCOL_INFO protocolInfo;
        int protocolInfoSize = sizeof(protocolInfo);

        if (0 != getsockopt(m_portSocket, SOL_SOCKET, SO_PROTOCOL_INFO,
                            (char*)&protocolInfo, &protocolInfoSize))
        {
            return;
        }
        SOCKADDR_BTH address;
        address.addressFamily = AF_BTH;
        address.btAddr = 0;
        address.serviceClassId = GUID_NULL;
        address.port = BT_PORT_ANY;
        sockaddr *pAddr = (sockaddr*)&address;

        if (0 != bind(m_portSocket, pAddr, sizeof(SOCKADDR_BTH)))
        {
            emit messToLog(tr("%1").arg(GetLastError()));
        }
        else
        {
            emit messToLog(tr("Binded"));
            int length = sizeof(SOCKADDR_BTH) ;
            getsockname(m_portSocket,(sockaddr*)&address,&length);
            emit messToLog(tr("Local Bluetooth device is %1%2  "
                           "Server channel = %3")
                        .arg(GET_NAP(address.btAddr), 4, 16)
                        .arg(GET_SAP(address.btAddr), 8, 16)
                        .arg(address.port));
        }

        int size = sizeof(SOCKADDR_BTH);
        if (0 != getsockname(m_portSocket, pAddr, &size))
        {
            emit messToLog(QString::number(GetLastError()));
        }
        if (0 != listen(m_portSocket, 10))
        {
            emit messToLog(QString::number(GetLastError()));
        }

        memset(&m_service, 0, sizeof(m_service));
        m_service.dwSize = sizeof(m_service);
        m_service.lpszServiceInstanceName = L"Command Data...";
        m_service.lpszComment = L"Pushing commands to PC";

        GUID serviceID = SerialPortServiceClass_UUID;

        m_service.lpServiceClassId = &serviceID;
        m_service.dwNumberOfCsAddrs = 1;
        m_service.dwNameSpace = NS_BTH;

        CSADDR_INFO csAddr;
        memset(&csAddr, 0, sizeof(csAddr));
        csAddr.LocalAddr.iSockaddrLength = sizeof(SOCKADDR_BTH);
        csAddr.LocalAddr.lpSockaddr = pAddr;
        csAddr.iSocketType = SOCK_STREAM;
        csAddr.iProtocol = BTHPROTO_RFCOMM;
        m_service.lpcsaBuffer = &csAddr;

        if (0 != WSASetService(&m_service, RNRSERVICE_REGISTER, 0))
        {
            emit messToLog(tr("Service registration failed...."));
            emit messToLog(QString::number(GetLastError()));
        }
        else
        {
            emit messToLog(tr("Service registered."));
        }
        emit messToLog(tr("Wait for accept."));
        SOCKADDR_BTH sab2;
        int ilen = sizeof(sab2);
        m_transfSocket = accept (m_portSocket,(sockaddr*)&sab2, &ilen);
        if (m_transfSocket == INVALID_SOCKET)
        {
            emit messToLog(tr("Socket bind, error %1").arg(WSAGetLastError ()));
        }
        else
        {
            emit messToLog(tr("Connection came from %1%2 to channel %3")
                      .arg(GET_NAP(sab2.btAddr), 4, 16)
                      .arg(GET_SAP(sab2.btAddr), 8, 16)
                      .arg(sab2.port));
            emit messToLog(tr("Accepted."));

            emit accepted();
        }
    }
}

void SocketProvider::closeProvider()
{
    closesocket(m_transfSocket);
    if (0 != WSASetService(&m_service, RNRSERVICE_DELETE, 0))
    {
        emit messToLog(QString::number(GetLastError()));
    }
    else
    {
        emit messToLog(tr("Sockets closed."));
    }
    closesocket(m_portSocket);
    WSACleanup();
}

void SocketProvider::listenSocket()
{
    emit messToLog("Waiting for commands.");
    while(m_working)
    {
        char buffer[1024] = {0};
        memset(buffer, 0, sizeof(buffer));
        recv(m_transfSocket,(char*)buffer, sizeof(buffer), 0);
        emit messToLog(tr(buffer));
        QString s((char*)buffer);
        emit commandReceived(s);
        if(s.isEmpty())
        {
            emit messToLog("Disconnected.");
            m_working = false;
            break;
        }
    }
    closeProvider();
    emit closed();
}

void SocketProvider::listenSocketStop()
{
    m_working = false;
}

