#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QSystemTrayIcon>

#include <socketprovider.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void pressKey(uchar code, int timeDelay);
    void downKey(uchar code);
    void upKey(uchar code);

private slots:
    void parseCommand(QString comm);
    void toLog(QString mess);
    void resetService();

    void changeEvent(QEvent* event);
    void trayIconActivated(QSystemTrayIcon::ActivationReason reason);
    void trayActionExecute();
    void setTrayIconActions();
    void showTrayIcon();


private:
    Ui::MainWindow *ui;
    QMenu* m_trayMenu;
    QAction* m_minAction;
    QAction* m_restoreAction;
    QAction* m_resetAction;
    QAction* m_quitAction;
    QSystemTrayIcon* m_trayIcon;

    QThread m_providerThread;
    SocketProvider* m_provider;
};

#endif // MAINWINDOW_H
