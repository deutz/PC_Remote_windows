#-------------------------------------------------
#
# Project created by QtCreator 2016-01-13T14:09:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PC_Remote_Windows
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    socketprovider.cpp

HEADERS  += mainwindow.h \
    socketprovider.h

FORMS    += mainwindow.ui

LIBS += -lws2_32 -lBthprops -luuid

RESOURCES += \
    pics.qrc
